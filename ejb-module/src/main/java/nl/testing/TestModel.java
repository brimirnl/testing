package nl.testing;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TestModel {
    private String name;
    @JsonIgnore
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
