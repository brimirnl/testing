package nl.testing;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.stream.Stream;

@Path("/boe")
public class TestController {
    @GET
    public TestModel test() {
        final TestModel model = new TestModel();
        model.setName("test");
        model.setDescription("desc");
        Stream.of(model.getClass().getDeclaredFields())
                .peek(e-> System.out.println(e.getName()))
                .flatMap(e -> Stream.of(e.getAnnotations()))
                .forEach(e -> System.out.println(e.annotationType().getName()));

        return model;
    }
}
